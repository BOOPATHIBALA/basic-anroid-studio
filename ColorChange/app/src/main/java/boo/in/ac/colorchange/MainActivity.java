package boo.in.ac.colorchange;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

            TextView t;
            Button b;
            int ch=1;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t =findViewById(R.id.editText);
        b =findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(ch){
                    case 1:
                        t.setTextColor(Color.CYAN);
                        break;
                    case 2:
                        t.setTextColor(Color.RED);
                        break;
                    case 3:
                        t.setTextColor(Color.YELLOW);
                        break;
                    case 4:
                        t.setTextColor(Color.GRAY);
                        break;
                    case 5:
                        t.setTextColor(Color.GREEN);
                        break;
                    case 6:
                        t.setTextColor(Color.BLACK);
                        break;
                }
                ch++;
                if(ch==7)
                    ch=1;
            }
        });

    }
}